using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LongClickButton : MonoBehaviour , IPointerDownHandler , IPointerUpHandler
{
    private bool pointerDown;
    private float pointerDownTimer;

    [SerializeField] private float holdTime;

    public UnityEvent OnLongClick;

    [SerializeField] private Image fillImg;


    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Reset();

    }

    private void Update()
    {
        
        if (!RandomScrollManager.Instance.LongClickCheck)
        {
            if (pointerDown)
            {
                StartCoroutine(DelayClick());
            }
            else
            {
                Reset();
                StopAllCoroutines();
            }
        }
        
    }

    private IEnumerator DelayClick()
    {
        yield return new WaitForSeconds(0.25f);
        pointerDownTimer += Time.deltaTime;
        if (pointerDownTimer >= holdTime)
        {
            OnLongClick?.Invoke();
            Reset();
        }
        fillImg.fillAmount = pointerDownTimer / holdTime;
    }

    private void Reset()
    {
        pointerDown = false;
        pointerDownTimer = 0;
        fillImg.fillAmount = pointerDownTimer / holdTime;
    }

    public void DestroyCard()
    {
        switch (tag)
        {
            case "Card_HeroA":
                InventoryManager.Instance.Cards[0].CardAmount--;
                break;
            case "Card_HeroB":
                InventoryManager.Instance.Cards[1].CardAmount--;
                break;
            case "Card_HeroC":
                InventoryManager.Instance.Cards[2].CardAmount--;
                break;
            case "Card_SubHeroA":
                InventoryManager.Instance.Cards[3].CardAmount--;
                break;
            case "Card_SubHeroB":
                InventoryManager.Instance.Cards[4].CardAmount--;
                break;
            case "Card_SubHeroC":
                InventoryManager.Instance.Cards[5].CardAmount--;
                break;
            case "Card_SubHeroD":
                InventoryManager.Instance.Cards[6].CardAmount--;
                break;
        }

        if (CompareTag("Card_HeroA") || CompareTag("Card_HeroB") || CompareTag("Card_HeroC"))
        {
            MoneyManager.Instance.AddMoney(MoneyManager.Instance.DefaultSellHeroCost);
        }
        else
        {
            MoneyManager.Instance.AddMoney(MoneyManager.Instance.DefaultSellSubHeroCost);

        }
        
        UiManager.Instance.UpdateCardInventory();

        UiManager.Instance.blockUI.gameObject.SetActive(false);
        AnimationManager.Instance.MapDown();
        InventoryManager.Instance.IsBuildMode = false;
        BuildManager.Instance.heroToBuild = InventoryManager.Instance.HeroTeam.Find(x=>x.HeroName == HeroName.Null);
        Destroy(gameObject);
    }
}
