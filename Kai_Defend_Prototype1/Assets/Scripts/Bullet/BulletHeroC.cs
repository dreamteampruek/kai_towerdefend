using System.Collections;
using System.Collections.Generic;
using Bullet;
using Manager;
using UnityEngine;

public class BulletHeroC : BaseBullet
{
    private void Start()
    {
        hero = GetHeroBluePrint(HeroName.W410_Odin);
        GetStatus();
    }

    private void Update()
    {
        MoveToTarget();
    }

    private new void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();;
    }
}
