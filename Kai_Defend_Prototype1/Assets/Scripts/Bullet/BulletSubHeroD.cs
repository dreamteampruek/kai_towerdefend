﻿using Manager;

namespace Bullet
{
    public class BulletSubHeroD : BaseBullet
    {
        private void Start()
        {
            hero = GetHeroBluePrint(HeroName.Z46_Genesis);
            GetStatus();
        }

        private void Update()
        {
            MoveToTarget();
        }

        private new void OnDrawGizmosSelected()
        {
            base.OnDrawGizmosSelected();;
        }
    }
}