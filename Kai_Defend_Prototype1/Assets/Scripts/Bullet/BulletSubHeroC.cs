using System.Collections;
using System.Collections.Generic;
using Bullet;
using Manager;
using UnityEngine;

public class BulletSubHeroC : BaseBullet
{
    private void Start()
    {
        hero = GetHeroBluePrint(HeroName.Z25_Terror);
        GetStatus();
    }

    private void Update()
    {
        MoveToTarget();
    }

    private new void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();;
    }
}
