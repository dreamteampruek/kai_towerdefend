using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Manager;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Map
{
    public class Area : MonoBehaviour
    {
        // public static Area Instance { get; private set; }
        [SerializeField] private Color hoverColor;
        [SerializeField] private Color startColor;
        [SerializeField] private Color MoreThanHeroLimitColor;
        public static float CurrentHeroAmount { get; private set; }
        public static float CurrentSubHeroAmount { get; private set; }

        [HideInInspector]
        public GameObject hero;

        [HideInInspector] 
        public HeroBluePrint HeroBluePrint;
        
        private Renderer render;


        private bool moreThanHeroLimit => CurrentHeroAmount >= BuildManager.Instance.HeroLimit;
        private bool moreThanSubHeroLimit => CurrentSubHeroAmount >= BuildManager.Instance.SubHeroLimit;
        
        private void Start()
        {
            CurrentHeroAmount = 0;
            CurrentSubHeroAmount = 0;
            render = GetComponent<Renderer>();
            startColor = render.material.color;
        }

        public Vector3 GetPosition()
        {
            return transform.position + new Vector3(0f, -0.1f, 0f);
            //new Vector3(0f, 1.2f - 0.31f, 0f);
            //0.31f is the difference in the height of the stage when play click to place hero
        }

        private Vector3 GetEvoPosition()
        {
            return transform.position + new Vector3(0f, +0.2f, 0f);
            //0.31f is the difference in the height of the stage when play click to place hero
        }

        private void BuildHero(HeroBluePrint blueprint)
        {
            //TODO: Check amount of Hero Can Build Here
            if (blueprint.IsBuilt)
            {
                Debug.Log("This Hero Landed");
                return;
            }
            
            if (moreThanHeroLimit)
            {
                if (blueprint.IsHero)
                {
                    Debug.Log("Can't build anymore");
                    return;
                }
            }
            else if (moreThanSubHeroLimit)
            {
                if (!blueprint.IsHero)
                {
                    Debug.Log("Can't sub build anymore");
                    return;
                }
            }
            
            if (moreThanHeroLimit && moreThanSubHeroLimit)
            {
                return;
            }
            
            if (blueprint.IsHero)
            {
                CurrentHeroAmount++;
            }
            
            else
            {
                CurrentSubHeroAmount++;
            }

            HeroBluePrint = blueprint;
            
            var _hero = Instantiate(blueprint.Prefab, GetPosition(), Quaternion.identity);
            hero = _hero;
            ChainSkill.Instance.AddHeroToField(blueprint);
            blueprint.IsBuilt = true;
        }

        public void UpgradeHero()
        {
            MoneyManager.Instance.DeleteMoney(HeroBluePrint.UpgradeCost);
            HeroBluePrint.Level++;
            HeroBluePrint.AttackDamage += 0.05f;//Refactor Magic number
            HeroBluePrint.AttackSpeed += 0.05f;
            HeroBluePrint.HeroCostAmount += HeroBluePrint.UpgradeCost;
            
            GameManager.Instance.UpdateHeroInfo();
        }

        public void EvoHero()
        {
            Destroy(hero);
            if (!HeroBluePrint.IsHero)
            {
                var subHeroEvo = Instantiate(HeroBluePrint.EvoPrefab, GetEvoPosition(), Quaternion.identity);
                hero = subHeroEvo;
                HeroBluePrint.IsEvo = true;
                return;
            }
            
            if (HeroBluePrint.IsPreEvo)
            {
                var heroEvo2 = Instantiate(HeroBluePrint.Evo2Prefab, GetEvoPosition(), Quaternion.identity);
                hero = heroEvo2;
                HeroBluePrint.IsEvo = true;
                return;
            }
            
            var heroEvo1 = Instantiate(HeroBluePrint.EvoPrefab, GetEvoPosition(), Quaternion.identity);
            hero = heroEvo1;
            HeroBluePrint.IsPreEvo = true;
            
        }

        public void SellHero()
        {
            if (HeroBluePrint.IsHero)
            {
                MoneyManager.Instance.AddMoney(HeroBluePrint.GetSellAmount() + BuildManager.Instance.HeroSellCost);
                CurrentHeroAmount--;
            }
            else
            {
                MoneyManager.Instance.AddMoney(BuildManager.Instance.SubHeroSellCost);
                CurrentSubHeroAmount--;
            }

            ChainSkill.Instance.RemoveHeroInField(HeroBluePrint);
            
            HeroBluePrint.IsBuilt = !true;
            HeroBluePrint.HeroCostAmount = 0;
            HeroBluePrint.Level = 0;
            Destroy(hero);
            HeroBluePrint = null;
            var debugNull = InventoryManager.Instance.HeroTeam.FirstOrDefault(hero => hero.HeroName == HeroName.Null);

            BuildManager.Instance.heroToBuild = debugNull; //I don't know why but it can debug many null ref for bullet
        }

        private void OnMouseDown()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            if (hero != null && InventoryManager.Instance.IsBuildMode == false)
            {
                BuildManager.Instance.SelectHero(this);
                return;
            }
            
            if (hero != null)
            {
                Debug.Log("Test2");
                return;
            }
            
            if (!BuildManager.Instance.CanBuild)
            {
                Debug.Log("Please Check Hero Prefab");
                return;
            }
            
            

            BuildHero(BuildManager.Instance.GetHeroToBuild());

            if (InventoryManager.Instance.UsedCardTemp != null)
            {
                Destroy(InventoryManager.Instance.UsedCardTemp.gameObject);
            }

            // foreach (var card in InventoryManager.Instance.CardsInventory)
            // {
            //     if (card == null)
            //     {
            //         continue;
            //     }
            //     card.interactable = true;
            // }
            
            UiManager.Instance.blockUI.gameObject.SetActive(false);
            AnimationManager.Instance.MapDown();
            InventoryManager.Instance.IsBuildMode = false;
            
            InventoryManager.Instance.UsedCardTempPanel.gameObject.SetActive(false);
            
            UiManager.Instance.UpdateCardInventory();
            
            UiManager.Instance.RandomScrollButtonArea.gameObject.SetActive(!false);
            InventoryManager.Instance.InventoryButton.gameObject.SetActive(true);
        }
    }
}