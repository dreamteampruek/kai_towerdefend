using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class RankData : MonoBehaviour
{
    [Header("Data")]
    public string PlayerName;
    public int RankNumber;
    public Sprite PlayerImage;
    public int PlayerScore;

    [Header("UI")]
    public TextMeshProUGUI NumberText;
    public Image ProfileImage;
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI ScoreText;

    public RankData(int rankNumber, string playerName, int playerScore)
    {
        RankNumber = rankNumber;
        PlayerName = playerName;
        PlayerScore = playerScore;
    }

    public void UpdateRankData()
    {
        NumberText.text = RankNumber.ToString();
        NameText.text = PlayerName;
        ScoreText.text = PlayerScore.ToString();

    }
}
