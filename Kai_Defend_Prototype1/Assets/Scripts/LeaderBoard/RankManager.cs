using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankManager : MonoBehaviour
{
    public GameObject RankDataPrefap;
    public Transform RankPanel;
    public Transform LeaderBoardPanel;
    public List<RankData> PlayerDatas;

    private void Start()
    {
        LeaderBoardPanel.gameObject.SetActive(!true);
    }

    public void CreateData()
    {
        var rankObj = Instantiate(RankDataPrefap, RankPanel);
        var rankData = rankObj.GetComponent<RankData>();
        rankData = new RankData(1, "Name", 1);
        rankData.UpdateRankData();
    }

    public void ShowLeaderBoardUI()
    {
        LeaderBoardPanel.gameObject.SetActive(true);
    }
}
