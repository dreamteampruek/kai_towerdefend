using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using SimpleJSON;
using UnityEngine.PlayerLoop;

#region User

[System.Serializable]
public class User
{
    [System.Serializable]
    public class UserData
    {
        public string name;
        public string gender;
        public int age;

        public UserData(string name, string gender, int age)
        {
            this.name = name;
            this.gender = gender;
            this.age = age;
        }
    }

    public List<UserData> userData;
}

#endregion

public class TestFireBase : MonoBehaviour
{
    public User user;

    public string url = "https://kaid-65c39-default-rtdb.asia-southeast1.firebasedatabase.app/";
    public string secret = "XbEOOhSIH7Igbe0rxm4xVrNbLbDkBJbbjaDWeV5n";
    
    
    public void GetData()
    {
        var urlData = url + "user/userData.json?auth=" + secret;

        user.userData = new List<User.UserData>();
        
        RestClient.Get(urlData).Then(response =>
        {
            JSONNode jsonNode = JSONNode.Parse(response.Text);

            for (var i = 0; i < jsonNode.Count; i++)
            {
                user.userData.Add(new User.UserData(jsonNode[i]["gender"],jsonNode[i]["name"], jsonNode[i]["age"]));
            }
            Debug.Log(jsonNode);
        }).Catch(error =>
        {
            Debug.Log("Error");
        });
    }

    public void PostData()
    {
        var urlData = url + "user/userData.json?auth=" + secret;
        RestClient.Get(urlData).Then(response =>
        {
            RestClient.Put<User>(urlData, user).Then(response =>
            {
                Debug.Log("Upload Data");
            }).Catch(error =>
            {
                Debug.Log("Error On Put");
            });
        }).Catch(error =>
        {
            Debug.Log("Error On Get");
        });
            
            
    }
}
