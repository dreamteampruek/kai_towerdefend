using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using TMPro;
using UnityEngine;

[System.Serializable]
public class SkillBluePrint
{
    [HideInInspector]
    [SerializeField] private string name;
    
    public BuildName BuildName;
    
    [Header("HeroRequire")] 
    public List<HeroName> HeroRequirements;
    public List<Buff> Buffs; 
    public bool IsActive;
    public bool IsShowOnUI;

    [Header("Status Boost")]
    public float DamageBoost;
    public float AttackSpeedBoost;
    public float AttackRangeBoost;
    public float AoeBoost;

}

public enum BuildName
{
    BuildDamageBoost,
    BuildAttackSpeedBoost,
    BuildAttackRangeBoost,
    BuildAoeDamage
}

public enum Buff
{
    DamageBoost,
    AttackSpeedBoost,
    CriticalBoost,
    AttackRange,
    ManaBoost,
    AoeBuff
}