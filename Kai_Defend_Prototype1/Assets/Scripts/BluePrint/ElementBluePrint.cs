using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ElementBluePrint
{
    [Header("6 Fire Status Boost")]
    public float Fire6_CriticalRate;

    [Header("4 Fire Status Boost")]
    public float Fire4_ReduceEnemySpeed;
   
    [Header("2 Fire Status Boost")]
    public float Fire2_FireDamage;
    
    [Header("6 Water Status Boost")]
    public float Water6_Buff_Aoe;
    
    [Header("4 Water Status Boost")]
    public float Water4_AddGoldDrop;
   
    [Header("2 Water Status Boost")]
    public float Water2_WaterDamage;
    
    [Header("6 Plant Status Boost")]
    public float Plant6_AddDamage;
    
    [Header("4 Plant Status Boost")]
    public float Plant4_AddSpeedAttack;
   
    [Header("2 Plant Status Boost")]
    public float Plant2_PlantDamage;

    [Header("Element Setting")] 
    public float BetterElement;
}
