using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    [System.Serializable]
    public class HeroBluePrint
    {
        public string Name;
        [Header("Hero Name")]
        public HeroName HeroName;
        
        [Header("Attack Type")]
        public AttackType AttackType;
        
        [Header("Prefab")]
        public GameObject Prefab;
        public GameObject EvoPrefab;
        public GameObject Evo2Prefab;
        
        [Header("Element")]
        public Element ElementType;

        [Header("CheckList")]
        public bool IsHero;
        [HideInInspector]
        public bool IsBuilt;
        [HideInInspector]
        public bool IsEvo;
        [HideInInspector]
        public bool IsPreEvo;

        [Header("Laser Tower")]
        public float DamageOverTime; 
        public float SlowPercent = 0.3f;
        
        [Header("Status")]
        public float BulletSpeed = 20f;
        public float AttackDamage;
        public float AttackSpeed;
        public float AttackRange;
        public float AoeRange = 0f;
        public int UpgradeCost = 100;

        [Header("Upgrade Machine Status")] [Header("For Hero")]
        public float Upgrade_Machine_AttackDamage;
        public float Upgrade_Machine_AttackSpeed;
        public float Upgrade_Machine_AttackRange;
        public int Upgrade_Machine_UpgradeCost;
        
        [Header("Upgrade Missile Status")]
        public float Upgrade_Missile_AttackDamage;
        public float Upgrade_Missile_AttackRange;
        public float Upgrade_Missile_AoeRange = 0f;
        public int Upgrade_Missile_UpgradeCost;
        
        [Header("Upgrade Laser Status")]
        public float Upgrade_Laser_DamageOverTime; 
        public int Upgrade_Laser_UpgradeCost;
        
        
        [HideInInspector] 
        public int Level;

        [HideInInspector]
        public int HeroCostAmount;
        
        public int GetSellAmount()
        {
            return (HeroCostAmount / 2);
        }
    }
    
    public enum HeroName
    {
        P32_Enterprise,
        F13_Colorado,
        TEP58_Interpid,
        W410_Odin,
        Z1_Hornet,
        Z23_Glorious,
        Z25_Terror,
        Z46_Genesis,
        Null
    }

    public enum Element
    {
        Fire,
        Water,
        Plant,
        Null
    }
    
    public enum AttackType
    {
        MachineGun,
        Missile,
        Laser
    }
}

