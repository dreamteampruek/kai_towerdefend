using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Manager;
using UnityEngine;

public class ChainSkill : MonoBehaviour
{
    public static ChainSkill Instance { get; private set; }
    public List<SkillBluePrint> skillChains;
    public ElementBluePrint elementChain;

    public List<HeroBluePrint> HeroInField;//if Raider didn't say to change to private dont change okaY? it'll null ref
    
    public int FireElementCount;
    public int WaterElementCount;
    public int PlantElementCount;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void CheckSkillChainActive()
    {
        foreach (var skillChain in skillChains)
        {
            if (skillChain.IsActive && !skillChain.IsShowOnUI)
            {
                if (skillChain.IsActive && skillChain.IsShowOnUI)
                {
                    continue;
                }

                SetBuff(skillChain);
                
                foreach (var buff in skillChain.Buffs)
                {
                    UiManager.Instance.SpawnChainSkillText(buff.ToString()); //Generate all buff
                }
                
                skillChain.IsShowOnUI = true;
            }
            else if (!skillChain.IsActive && skillChain.IsShowOnUI)
            {
                if (!skillChain.IsActive && !skillChain.IsShowOnUI)
                {
                    continue;
                }
                
                RemoveBuff(skillChain);
                
                foreach (var buff in skillChain.Buffs)
                {
                    UiManager.Instance.RemoveChainSkillText(buff.ToString()); //Remove all buff
                }
                
                skillChain.IsShowOnUI = false;
            }
        }
    }

    #region Main event //Refactor next time
    
    
    public void AddHeroToField(HeroBluePrint blueprint)
    {
        HeroInField.Add(blueprint);

        foreach (var skillChain in skillChains)
        {
            FindBuild(HeroInField, skillChain, skillChain.HeroRequirements);
        }

        CheckSkillChainActive();
        FindElementBuild();
    }

    #endregion
    
    public void RemoveHeroInField(HeroBluePrint blueprint)
    {
        HeroInField.Remove(blueprint);
        foreach (var skillChain in skillChains)
        {
            FindBuild(HeroInField, skillChain, skillChain.HeroRequirements);
        }

        CheckSkillChainActive();
        FindElementBuild();
    }
    

    private SkillBluePrint GetBuild(BuildName buildName) //TODO: For Implement Maybe?
    {
        SkillBluePrint temp = null;
        foreach (var build in skillChains)
        {
            if (build.BuildName == buildName)
            {
                temp = build;
            }
        }
        return temp;
    }

    private void SetBuff(SkillBluePrint build)
    {
        foreach (var hero in InventoryManager.Instance.HeroTeam)
        {
                Debug.Log("Buff");
                hero.AttackDamage += build.DamageBoost;
                hero.AttackSpeed += build.AttackSpeedBoost;
                hero.AttackRange += build.AttackRangeBoost;
                hero.AoeRange += build.AoeBoost; 
        }
    }
    
    private void RemoveBuff(SkillBluePrint build)
    {
        foreach (var hero in InventoryManager.Instance.HeroTeam)
        {
            Debug.Log("RemoveBuff");
            hero.AttackDamage -= build.DamageBoost;
            hero.AttackSpeed -= build.AttackSpeedBoost;
            hero.AttackRange -= build.AttackRangeBoost;
            hero.AoeRange -= build.AoeBoost; 
        }
    }

    private void FindBuild (List<HeroBluePrint> heros, SkillBluePrint skill,List<HeroName> heroRequires)
    {
        var hero1Detected = heros.Any(x => x.HeroName == heroRequires[0]);
        var hero2Detected = heros.Any(x => x.HeroName == heroRequires[1]);
        //TODO: If want to refactor this code try List.FindAll

        if (hero1Detected && hero2Detected)
        {
            skill.IsActive = true;
        }
        else
        {
            skill.IsActive = false;
        }
    }
    
    //Element
    private void FindElementBuild ()
    {
        
        var fireElement = HeroInField.FindAll(x => x.ElementType == Element.Fire);

        var waterElement = HeroInField.FindAll(x => x.ElementType == Element.Water);

        var plantElement = HeroInField.FindAll(x => x.ElementType == Element.Plant);

        FireElementCount = fireElement.Count;
        WaterElementCount = waterElement.Count;
        PlantElementCount = plantElement.Count;
        
    }
}


