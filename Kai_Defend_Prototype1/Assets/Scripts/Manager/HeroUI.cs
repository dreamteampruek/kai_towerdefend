using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Manager;
using Map;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeroUI : MonoBehaviour
{
    [SerializeField] private GameObject heroUI;
    private Area target;
    
    [SerializeField] private TextMeshProUGUI sellCostText;
    [SerializeField] private TextMeshProUGUI evoCostText;
    
    [SerializeField] private RectTransform levelPanel;
    [SerializeField] private Slider upgradeSlider;
    [SerializeField] private TextMeshProUGUI upgradeCostText;
    [SerializeField] private TextMeshProUGUI levelText;
    
    [SerializeField] private TextMeshProUGUI heroLevelText;
    
    [SerializeField] private Button upgradeButton;
    [SerializeField] private Button evoButton;
    
    [SerializeField] private RectTransform attackRangeImg;
    
    [SerializeField] private GameObject rotateObj;
    
    [SerializeField] private TextMeshProUGUI heroNameText;
    [SerializeField] private TextMeshProUGUI subHeroNameText;
    [SerializeField] private TextMeshProUGUI heroElementTypeText;

    private Rect spriteRect;
    private int tempLevel;

    private void Awake()
    {
        HideHeroUI();
    }

    private void Update()
    {
        rotateObj.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
    }

    public void SetTarget(Area _target)
    {
        levelPanel.gameObject.SetActive(!true);
        upgradeSlider.SetValueWithoutNotify(0);

        target = _target;
        BuildManager.Instance.heroToBuild = target.HeroBluePrint; //for Update Hero Status before Evo
        ShowAttackRange();

        var getHeroCard = InventoryManager.Instance.Cards.Find(x => x.CardName == target.HeroBluePrint.HeroName);
        CheckHeroType();
        transform.position = target.GetPosition();
        upgradeCostText.text = "0 G";
        heroElementTypeText.text = $"Element : {target.HeroBluePrint.ElementType.ToString()}";
        if (target.HeroBluePrint.IsHero)
        {
            sellCostText.text = $"{target.HeroBluePrint.GetSellAmount() + BuildManager.Instance.HeroSellCost} G"; 
            heroLevelText.text = $"Lv.{target.HeroBluePrint.Level}";
            heroNameText.text = $"{target.HeroBluePrint.HeroName.ToString()}";
            subHeroNameText.gameObject.SetActive(false);
            heroLevelText.gameObject.SetActive(true);
            heroNameText.gameObject.SetActive(true);
        }
        else
        {
            sellCostText.text = $"{BuildManager.Instance.SubHeroSellCost} G";
            subHeroNameText.text = $"{target.HeroBluePrint.HeroName.ToString()}";
            heroLevelText.gameObject.SetActive(false);
            heroNameText.gameObject.SetActive(false);
            subHeroNameText.gameObject.SetActive(true);
        }

        if (!target.HeroBluePrint.IsEvo)
        {
            evoCostText.text = $"{getHeroCard.CardAmount} / 3 ea.";
            evoButton.interactable = true;
        }
        else
        {
            evoCostText.text = "Max Evo!";
            evoButton.interactable = !true;
        }

        heroUI.SetActive(true);
    }

    public void HideHeroUI()
    {
        levelPanel.gameObject.SetActive(!true);
        heroUI.SetActive(false);
    }
    
    private IEnumerator ChangeButtonColor()
    {
       upgradeButton.image.color = Color.red;
       yield return new WaitForSeconds (0.05f);
       upgradeButton.image.color = Color.white;
    }
    
    public void Upgrade()
    {
        if (MoneyManager.Instance.Money < target.HeroBluePrint.UpgradeCost)
        {
            StartCoroutine(ChangeButtonColor());
            return;
        }
        levelPanel.gameObject.SetActive(true);
        levelText.text = $"Lv. {target.HeroBluePrint.Level + upgradeSlider.value}";
    }

    public void UpdateSlider()
    {
        UpdateLevel();
        upgradeSlider.maxValue = tempLevel;
        levelText.text = $"Lv. {target.HeroBluePrint.Level + upgradeSlider.value}";
        upgradeCostText.text = $"{target.HeroBluePrint.UpgradeCost * upgradeSlider.value} G";
    }

    public void UpgradeHero()
    {
        for (var i = 0; i < upgradeSlider.value; i++)
        {
            target.UpgradeHero();
        }
        levelPanel.gameObject.SetActive(!true);
        heroLevelText.text = $"Lv. {target.HeroBluePrint.Level}";
        upgradeSlider.SetValueWithoutNotify(0);
        sellCostText.text = $"{target.HeroBluePrint.GetSellAmount() + BuildManager.Instance.HeroSellCost} G";
        upgradeCostText.text = "0 G";
    }

    private void UpdateLevel()
    {
        var temp = MoneyManager.Instance.Money;
        tempLevel = 0;
        while (temp >= target.HeroBluePrint.UpgradeCost)
        {
            tempLevel++;
            temp -= target.HeroBluePrint.UpgradeCost;
        }
    }

    public void EvoHero()
    {
        int listCount;
        var checkloop = 0;
        var getHeroCard = InventoryManager.Instance.Cards.Find(x => x.CardName == target.HeroBluePrint.HeroName);
        if (getHeroCard.CardAmount >= 3)
        {
            getHeroCard.CardAmount -= 3;

            listCount = getHeroCard.CardToEvo.Count;
            
            for (var i = 0; i < listCount; i++)
            {
                Debug.Log("Amount" + i);
                Debug.Log(getHeroCard.CardToEvo.Count);
                if (getHeroCard.CardToEvo[i] == null)
                {
                    getHeroCard.CardToEvo.Remove(getHeroCard.CardToEvo[i]);
                    listCount--;
                    i--;
                    
                }
                else
                {
                    Destroy(getHeroCard.CardToEvo[i].gameObject);
                    getHeroCard.CardToEvo.Remove(getHeroCard.CardToEvo[i]);
                    listCount--;
                    i--;
                    checkloop++;
                }

                if (checkloop >= 3)
                {
                    target.EvoHero();
                    BuildManager.Instance.DeselectHero();
                    return;
                }
            }
        }
    }

    public void SellHero()
    {
        for (var i = 0; i < target.HeroBluePrint.Level; i++)
        {
            target.HeroBluePrint.AttackDamage -= 0.05f;//Refactor Magic number
            target.HeroBluePrint.AttackSpeed -= 0.05f;
        }
        target.SellHero();
        BuildManager.Instance.DeselectHero();
    }

    private void CheckHeroType()
    {
        if (target.HeroBluePrint.IsHero == false)
        {
            upgradeButton.gameObject.SetActive(!true);
        }
        else
        {
            upgradeButton.gameObject.SetActive(true);
        }
    }
    
    private void ShowAttackRange()
    {
        attackRangeImg.localScale =
            new Vector3(target.HeroBluePrint.AttackRange / 2f, target.HeroBluePrint.AttackRange / 2f,0); // r=D/2 
    }




}
