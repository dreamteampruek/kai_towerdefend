using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Map;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = System.Random;

namespace Manager
{
    public class InventoryManager : MonoBehaviour
    {
        [Header("Hero Manage")]
        public List<HeroBluePrint> HeroTeam;

        [Header("Inventory")]
        public List<CardBluePrint> Cards;
        
        [Header("Inventory Optional")]
        public List<Button> CardsInventory;

        [Header("Inventory Setup")]
        [SerializeField] private RectTransform inventoryPanel;

        public RectTransform InventoryField;
        public RectTransform UsedCardTempPanel;
        public Button InventoryButton;
        [SerializeField] private RectTransform usedCardField;

        [HideInInspector] 
        public Button UsedCardTemp;

        [HideInInspector]
        public bool IsBuildMode;
        
        private bool toggle;

        private bool isInventoryFull;

        public static InventoryManager Instance { get; private set; }
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            UsedCardTempPanel.gameObject.SetActive(false);
            inventoryPanel.gameObject.SetActive(false);
            IsBuildMode = false;
            isInventoryFull = false;
            RandomScrollManager.Instance.AddCard += AddCardToInventory;
        }

        private void AddCardToInventory()
        {
            var count = CardsInventory.Count(item => item != null);
            if (count >= CardsInventory.Count)
            {
                Debug.Log("CARDFULL");
                return;
            }

            for (var i = 0; i < CardsInventory.Count; i++)
            {
                if (CardsInventory[i] == null)
                {
                    CardsInventory[i] = Instantiate(RandomScrollManager.Instance.CardTemp,InventoryField);
                    TagCardId(CardsInventory[i]);
                    UiManager.Instance.OnCloseRandomButtonClicked();
                }
                else
                {
                    continue;
                } 
                return;
            }
        }

        private void TagCardId(Button button)
        {
            if (RandomScrollManager.Instance.CardTemp.CompareTag("Card_HeroA"))
            {
                AddCard(HeroName.P32_Enterprise,Cards[0],button);
            }
            else if (RandomScrollManager.Instance.CardTemp.CompareTag("Card_HeroB"))
            {
                AddCard(HeroName.F13_Colorado,Cards[1],button);
            }
            else if (RandomScrollManager.Instance.CardTemp.CompareTag("Card_HeroC"))
            {
                AddCard(HeroName.W410_Odin,Cards[2],button);
            }
            else if (RandomScrollManager.Instance.CardTemp.CompareTag("Card_SubHeroA"))
            {
                AddCard(HeroName.Z1_Hornet,Cards[3],button);
            }
            else if (RandomScrollManager.Instance.CardTemp.CompareTag("Card_SubHeroB"))
            {
                AddCard(HeroName.Z23_Glorious,Cards[4],button);
            }
            else if (RandomScrollManager.Instance.CardTemp.CompareTag("Card_SubHeroC"))
            {
                AddCard(HeroName.Z25_Terror,Cards[5],button);
            }
            else if (RandomScrollManager.Instance.CardTemp.CompareTag("Card_SubHeroD"))
            {
                AddCard(HeroName.Z46_Genesis,Cards[6],button);
            }
        }

        private void AddCard(HeroName cardName,CardBluePrint card,Button button)
        {
            if (isInventoryFull)
            {
                return;
            }
            if (card.CardName == cardName)
            {
                card.Card = button;
                card.Card.onClick.AddListener(delegate { GetCard(cardName,card,button); });
                card.CardAmount++;
                card.CardToEvo.Add(button);
            }
        }
        
        private void GetCard(HeroName cardName,CardBluePrint card,Button button)
        {
            if (card.CardName == cardName)
            {
                PickHero(cardName);

                if (BuildManager.Instance.IsHeroBuilt)
                {
                    return; //tell Player herobuilt
                }

                if (card.IsHeroCard)
                {
                    if (Area.CurrentHeroAmount >= BuildManager.Instance.HeroLimit)
                    {
                        return; //tell Player Herofull
                    }
                }

                if (!card.IsHeroCard)
                {
                    if (Area.CurrentSubHeroAmount >= BuildManager.Instance.SubHeroLimit)
                    {
                        return; //tell Player SubHerofull
                    }
                }

                IsBuildMode = true;
                AnimationManager.Instance.MapUp();
                UiManager.Instance.blockUI.gameObject.SetActive(true);

                //To Hide other card when card has selected
                // var cardTemps = CardsInventory.FindAll(x => x.CompareTag($"Card_{cardName}"));
                // foreach (var cardTemp in cardTemps) 
                // {
                //     cardTemp.interactable = false;
                // }
                

                
                UsedCardTempPanel.gameObject.SetActive(true);



                UsedCardTemp = Instantiate(button,usedCardField);
                UsedCardTemp.onClick.RemoveAllListeners();
                UsedCardTemp.onClick.AddListener(delegate { CancelCardUsed(cardName,button,card); });

                card.CardAmount--;
                Destroy(button.gameObject);
                
                ShowInventoryUI();

                
            }
        }
        
        private void CancelCardUsed(HeroName cardName, Button button, CardBluePrint card)
        {
            UsedCardTempPanel.gameObject.SetActive(false);
            button.onClick.RemoveAllListeners();
            UsedCardTemp.onClick.RemoveAllListeners();

            UsedCardTemp.onClick.AddListener(delegate { GetCard(cardName,card,button); });
            ShowInventoryUI();
            
            RandomScrollManager.Instance.CardTemp = UsedCardTemp;
            AddCardToInventory();
            
            AnimationManager.Instance.MapDown();
            IsBuildMode = false;
            
            BuildManager.Instance.heroToBuild = HeroTeam.Find(x=>x.HeroName == HeroName.Null);
            UiManager.Instance.blockUI.gameObject.SetActive(!true);
            Destroy(UsedCardTemp.gameObject);
            UsedCardTemp = null;
        }

        private void GetHero(HeroName name)
        {
            //find hero in list
            var hero = HeroTeam.Find(x => x.HeroName == name);
            BuildManager.Instance.SetHero(hero);
        }
        
        private void PickHero(HeroName heroName)
        {
            if (heroName == HeroName.P32_Enterprise)
            {
                GetHero(HeroName.P32_Enterprise);
            }
            else if (heroName == HeroName.F13_Colorado)
            {
                GetHero(HeroName.F13_Colorado);
            }
            else if (heroName == HeroName.W410_Odin)
            {
                GetHero(HeroName.W410_Odin);  
            }
            else if (heroName == HeroName.Z1_Hornet)
            {
                GetHero(HeroName.Z1_Hornet);
            }
            else if (heroName == HeroName.Z23_Glorious)
            {
                GetHero(HeroName.Z23_Glorious);
            } 
            else if (heroName == HeroName.Z25_Terror)
            {
                GetHero(HeroName.Z25_Terror);
            } 
            else if (heroName == HeroName.Z46_Genesis)
            {
                GetHero(HeroName.Z46_Genesis);
            }
        }

        public void ShowInventoryUI()
        {
            UiManager.Instance.UpdateCardInventory();
            toggle = !toggle;
            if (toggle)
            {
                RandomScrollManager.Instance.HideRandomScollButton.Invoke();
                inventoryPanel.gameObject.SetActive(true);
                RandomScrollManager.Instance.LongClickCheck = false;
                BuildManager.Instance.DeselectHero();
            }
            else
            {
                RandomScrollManager.Instance.ShowRandomScollButton.Invoke();
                inventoryPanel.gameObject.SetActive(!true);

                if (UsedCardTempPanel.gameObject.activeInHierarchy)
                {
                    UiManager.Instance.RandomScrollButtonArea.gameObject.SetActive(false);
                    InventoryButton.gameObject.SetActive(false);
                    RandomScrollManager.Instance.LongClickCheck = true;
                }
                else
                {
                    UiManager.Instance.RandomScrollButtonArea.gameObject.SetActive(true);
                    InventoryButton.gameObject.SetActive(true);
                    RandomScrollManager.Instance.LongClickCheck = false;
                }

            }
        }
    }
}

