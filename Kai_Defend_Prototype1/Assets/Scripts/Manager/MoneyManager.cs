using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class MoneyManager : MonoBehaviour
    {
        public static MoneyManager Instance { get; private set; }
        
        public int Money { get; set; }
        [SerializeField] private int startMoney;
        [SerializeField] private int maxMoney;
        
        public int DefaultSellSubHeroCost = 125;
        public int DefaultSellHeroCost = 250;


        public void AddMoney(int amount)
        {
            if ((Money + amount) >= maxMoney)
            {
                Money = maxMoney;
                return;
            }
            Money += amount;
        }
        
        public void DeleteMoney(int amount)
        {
            Money -= amount;
        }
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            Money = startMoney;
        }
    }
}

