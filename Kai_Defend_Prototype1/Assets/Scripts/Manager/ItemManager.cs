using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour
{
    private float attackBoostTemp;
    
    [SerializeField] private int cookieTime;
    [SerializeField] private int candyTime;

    #region ItemFireWork

    [SerializeField] private List<float> damageTemp;
    [SerializeField] private List<float> speedTemp;


    private void Start()
    {
        AdsManager.Instance.OnAdsReward += FinishAdsReward;
    }

    private void FinishAdsReward()
    {
        FireWork();
    }

    public void FireWork()
    {
        var enemys = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in enemys)
        {
            GameManager.Instance.EnemyCount--;
            if (enemy.GetComponent<Chest>() != null)
            {
                continue;
            }
            Destroy(enemy.gameObject);
        }

    }
    #endregion

    #region Cookie

    public void Cookie()
    {
        foreach (var hero in InventoryManager.Instance.HeroTeam)
        {
            attackBoostTemp = (hero.AttackDamage * 30) / 100;
            damageTemp.Add(attackBoostTemp);
            hero.AttackDamage += attackBoostTemp;
            Debug.Log("Cookie Enable");
        }
        StartCoroutine(ActiveCookie30s());
    }

    private IEnumerator ActiveCookie30s()
    {
        yield return new WaitForSeconds(cookieTime);
        for (var i = 0; i < InventoryManager.Instance.HeroTeam.Count; i++)
        {
            InventoryManager.Instance.HeroTeam[i].AttackDamage -= damageTemp[i];
        }
        damageTemp.Clear();
        Debug.Log("Cookie Disable");
    }
    #endregion
    
    #region Cookie

    public void Candy()
    {
        foreach (var hero in InventoryManager.Instance.HeroTeam)
        {
            attackBoostTemp = (hero.AttackSpeed * 30) / 100;
            speedTemp.Add(attackBoostTemp);
            hero.AttackSpeed += attackBoostTemp;
        }
        Debug.Log("Candy Enable");

        StartCoroutine(ActiveCandy30s());
    }
    
    private IEnumerator ActiveCandy30s()
    {
        yield return new WaitForSeconds(candyTime);
        for (var i = 0; i < InventoryManager.Instance.HeroTeam.Count; i++)
        {
            InventoryManager.Instance.HeroTeam[i].AttackSpeed -= speedTemp[i];
        }
        speedTemp.Clear();
        Debug.Log("Candy Disable");
    }
    
    #endregion

    
    
    
}
