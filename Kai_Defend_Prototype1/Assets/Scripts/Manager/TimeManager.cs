using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public bool checkGameOver;
    
    [Range(0, 3)] [SerializeField] private float timeScale;
    [Range(0, 1)] [SerializeField] private float defaultTimeScale;
    // Start is called before the first frame update

    public static TimeManager Instance { get; private set; }

    private void Awake()
    {
        Debug.Assert(timeScale == 0, "timeScale is not zero");
        Debug.Assert(defaultTimeScale != 0, "defaultTimeScale is zero");

        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        GameManager.Instance.GameOver += True;
        AdsManager.Instance.OnAdsReward += False;
    }

    private void Update()
    {
        if (checkGameOver)
        {
            PauseGame();
        }
        else
        {
            UnPauseGame();
        }
        Debug.Log(checkGameOver);
    }


    // private void Update()
    // {
    //     //Check Is Player In game?
    //     if (UiManager.Instance.IsGameStart)
    //     {
    //         if (Input.GetKeyUp(KeyCode.Escape))
    //         {
    //             SoundManager.Instance.Play(SoundManager.Sound.ButtonClicked);
    //
    //             //Toggle button
    //             toggle = !toggle;
    //             //Pause && Unpause BGM
    //             SoundManager.Instance.Pause(SoundManager.Sound.MotorLoop, toggle);
    //             SoundManager.Instance.Pause(SoundManager.Sound.InGameBGM, toggle);
    //             SoundManager.Instance.Pause(SoundManager.Sound.Break, toggle);
    //             SoundManager.Instance.Pause(SoundManager.Sound.CarDrift, toggle);
    //             Time.timeScale = toggle ? timeScale : defaultTimeScale;
    //
    //             if (toggle)
    //             {
    //                 UiManager.Instance.ShowInGameMenu();
    //             }
    //             else
    //             {
    //                 UiManager.Instance.HideInGameMenu();
    //             }
    //         }
    //     }
    // }
    private void True()
    {
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        checkGameOver = true;
    }
    
    private IEnumerator Wait2()
    {
        yield return new WaitForSeconds(1);
        checkGameOver = false;
    }
    private void False()
    {
        StartCoroutine(Wait2());
    }
    
    private void PauseGame()
    {
        Time.timeScale = 0;
    }

    private void UnPauseGame()
    {
        //Toggle if other input call to unPause
        Time.timeScale = 1;
    }
    
    

    // public void UnPauseGame()
    // {
    //     //Toggle if other input call to unPause
    //     toggle = !toggle;
    //     SoundManager.Instance.Pause(SoundManager.Sound.InGameBGM, toggle);
    //     SoundManager.Instance.Pause(SoundManager.Sound.MotorLoop, toggle);
    //     SoundManager.Instance.Pause(SoundManager.Sound.Break, toggle);
    //     SoundManager.Instance.Pause(SoundManager.Sound.CarDrift, toggle);
    //     Time.timeScale = defaultTimeScale;
    // }
}