using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    public static AdsManager Instance { get; private set; } 

    private string gameId = "4416261";
    private bool testMode = true;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public event Action OnAdsReward;

    void Start()
    {
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, testMode);
    }

    public void ShowAds(string placement)
    {
        Advertisement.Show(placement);
    }

    public void OnUnityAdsReady(string placementId)
    {
    }

    public void OnUnityAdsDidError(string message)
    {
    }

    public void OnUnityAdsDidStart(string placementId)
    {
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        Debug.Log($"This {placementId} Finished");
        
        if (showResult == ShowResult.Finished)
        {
            Debug.Log("Reward Here!");
            OnAdsReward.Invoke();
            TimeManager.Instance.checkGameOver = false;
        }
        else if(showResult == ShowResult.Failed)
        {
            Debug.Log("Not Reward");
        }
    }
}
