using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Manager;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class RandomScrollManager : MonoBehaviour
{
    public static RandomScrollManager Instance { get; private set; }
    
    [SerializeField] private List<Button> heroPrefabs;
    [SerializeField] private List<Button> subHeroPrefabs;
    [Header("Setting")]
    [SerializeField] private float heroRandomRate = 0.8f;
    [SerializeField] private float subHeroRandomRate = 0.2f;
    [SerializeField] private float heroRandomPlusRate = 0.6f;
    [SerializeField] private float subHeroRandomPlusRate = 0.4f;
    [SerializeField] private RectTransform randomField;
    [SerializeField] private RectTransform randomArea;
    [SerializeField] private int maxCard = 3;
    [SerializeField] private int randomScrollCost = 50;
    [SerializeField] private int randomScrollPlusCost = 500;
    
    
    [SerializeField] private List<Button> cards;
    
    public UnityEvent HideRandomScollButton;
    public UnityEvent ShowRandomScollButton;

    public event Action OnFirstRandomCard;

    private bool isPickedFirstCard;

    [HideInInspector]
    public bool LongClickCheck;

    [HideInInspector]
    public Button CardTemp;

    public event Action AddCard;

    private void Awake()
    {
        if (Instance == null) 
        {
                Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        
        randomArea.gameObject.SetActive(true);

    }

    private void Start()
    {
        RandomCardOnStart();
    }

    private void RandomCardOnStart()
    {
        LongClickCheck = true;
        for (var i = 0; i < maxCard; i++)
        {
            cards.Add(Instantiate(heroPrefabs[Random.Range(0, heroPrefabs.Count)],randomField));
        }
        
        cards[0].onClick.AddListener(AddCard1);
        cards[1].onClick.AddListener(AddCard2);
        cards[2].onClick.AddListener(AddCard3);
        isPickedFirstCard = true;
    }

    private void ClearCard()
    {
        UiManager.Instance.UpdateCardInventory();

        foreach (var card in cards)
        {
            Destroy(card.gameObject);
        }
        cards.Clear();
    }

    private void RandomCard()
    {
        LongClickCheck = true;
        var cardCount = 0;
        if (!UiManager.Instance.IsRandomPlus)
        {
            if (MoneyManager.Instance.Money < randomScrollCost)
            {
                //not enough money
                return;
            }
            HideRandomScollButton.Invoke();
            MoneyManager.Instance.DeleteMoney(randomScrollCost);
            while(cardCount < maxCard)
            {
                if (Random.value > subHeroRandomRate) 
                {
                    cards.Add(Instantiate(subHeroPrefabs[Random.Range(0, subHeroPrefabs.Count)],randomField));
                    cardCount++;
                }
                else if(Random.value > heroRandomRate) 
                {
                    cards.Add(Instantiate(heroPrefabs[Random.Range(0, heroPrefabs.Count)],randomField));
                    cardCount++;
                }
            }
        }
        else
        {
            if (MoneyManager.Instance.Money < randomScrollPlusCost)
            {
                return;
            }
            HideRandomScollButton.Invoke();
            MoneyManager.Instance.DeleteMoney(randomScrollPlusCost);
            while(cardCount < maxCard)
            {
                if (Random.value > subHeroRandomPlusRate) 
                {
                    cards.Add(Instantiate(subHeroPrefabs[Random.Range(0, subHeroPrefabs.Count)],randomField));
                    cardCount++;
                }
                else if(Random.value > heroRandomPlusRate) 
                {
                    cards.Add(Instantiate(heroPrefabs[Random.Range(0, heroPrefabs.Count)],randomField));
                    cardCount++;
                }
            }
        }
        
        cards[0].onClick.AddListener(AddCard1);
        cards[1].onClick.AddListener(AddCard2);
        cards[2].onClick.AddListener(AddCard3);
    }

    public void ShowCard()
    {
        RandomCard();
        randomArea.gameObject.SetActive(true);
    }

    public void HideCard()
    {
        if (isPickedFirstCard)
        {
            OnFirstRandomCard.Invoke();
        }
        isPickedFirstCard = false;
        
        LongClickCheck = false;

        ClearCard();
        randomArea.gameObject.SetActive(!true);
    }

    public void ReRoll()
    {
        ClearCard();
        RandomCard();
    }

    private void AddCard1()
    {
        CardTemp = cards[0];
        AddCard.Invoke();
    }
    private void AddCard2()
    {
        CardTemp = cards[1];
        AddCard.Invoke();
    } 
    private void AddCard3()
    {
        CardTemp = cards[2];
        AddCard.Invoke();
    }
}