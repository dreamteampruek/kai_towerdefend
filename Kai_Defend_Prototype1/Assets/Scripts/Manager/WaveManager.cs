using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Manager
{
    public class WaveManager : MonoBehaviour
    {
        [SerializeField] private float timePerWaves;
        [SerializeField] private float timeCooldownWaves;
        [SerializeField] private float timer;

        private float timerTemp;
        
        private float cheatHpTemp;
        
        public float Countdown
        {
            get => timer;
            private set => timer = value;
        }
        
        //TODO: Implement : public event Action GameStarted;
        public event Action OnNextWave;

        private bool preparePhase;

        private double waveIndex;
        
        public static WaveManager Instance { get; private set; }

        private float cheatTemp;
        
        [HideInInspector]
        public int RandomEnemy;
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            RandomEnemy = UnityEngine.Random.Range(1, 4);
            GameManager.Instance.UpdateEnemyInfo();
            waveIndex = 1;
            preparePhase = true;
            timerTemp = timer;
        }

        private void Update()
        {
           WaveManage();
           WorldUIManager.Instance.WaveTimerUI.fillAmount = Countdown / timerTemp;
        }

        private void WaveManage()
        {
            if (Countdown <= 0f && preparePhase)
            {
                if (GameManager.Instance.ChestTemp != null)
                {
                    cheatHpTemp = GameManager.Instance.ChestTemp.EnemyHp;
                }
                
                switch (RandomEnemy)
                {
                    case 1 :
                        GameManager.Instance.Start_Colubted_Wave();
                        break;
                    case 2 :
                        GameManager.Instance.Start_Embio_Wave();
                        break;
                    case 3 :
                        GameManager.Instance.Start_Eclipseside_Wave();
                        break;
                    case 4 :
                        GameManager.Instance.Start_CrossDive_Wave();
                        break;
                    case 5 :
                        GameManager.Instance.Start_Shiro_Wave();
                        break;
                    case 6 :
                        GameManager.Instance.Start_Kana_Wave();
                        break;
                    case 7 :
                        GameManager.Instance.Start_Chest_Wave();
                        break;
                }
                WorldUIManager.Instance.WaveTextHolder.text = "Battle Phase";
                Countdown = timePerWaves;
                timerTemp = Countdown;
                preparePhase = false;
                waveIndex++; //dont forget to set wave = 1 when restart
            }
            else if (Countdown <= 0f && preparePhase == false)
            {
                if (GameManager.Instance.ChestTemp != null)
                {
                    var dif = cheatTemp - GameManager.Instance.ChestTemp.EnemyHp;
                    // MoneyManager.Instance.AddMoney((int)dif); //ค่ามันติดลบรึเปล่า?

                    Destroy(GameManager.Instance.ChestTemp.gameObject);
                    GameManager.Instance.ChestTemp = null;
                    //TODO : Implement Enemy Chest
                }
                if (waveIndex % 5 == 0 )
                {
                    RandomEnemy = 7;
                }
                else
                {
                    RandomEnemy = UnityEngine.Random.Range(1, 7);
                }
                UiManager.Instance.SetWaveCount($"{waveIndex}");
                
                GameManager.Instance.UpdateEnemyInfo();
                
                WorldUIManager.Instance.WaveTextHolder.text = "Prepare Phase";
                OnNextWave?.Invoke();
                GameManager.Instance.End_Wave();//instead of the End All wave 
                Countdown = timeCooldownWaves;
                timerTemp = Countdown;
                preparePhase = true;
            }

            Countdown -= Time.deltaTime;

            Countdown = Mathf.Clamp(Countdown, 0f, Mathf.Infinity);

            //TODO:Random Wave
        }
    }
}

