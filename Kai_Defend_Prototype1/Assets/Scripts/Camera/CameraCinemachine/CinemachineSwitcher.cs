using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Cinemachine;
using Manager;
using UnityEditor.Timeline;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CinemachineSwitcher : MonoBehaviour
{
    private Animator animator;

    [SerializeField] private Canvas canvas;
    private bool rightState;
    private bool leftState;
    private bool topState;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        canvas.gameObject.SetActive(true);
        RandomScrollManager.Instance.OnFirstRandomCard += HideCanvas;
    }

    private void HideCanvas()
    {
        canvas.gameObject.SetActive(!true);
    }

    public void SwitchLeftCamera()
    {
        if (topState)
        {
            return;
        }
        if (rightState)
        {
            rightState = false;
            animator.Play("Font");
            return;
        }
        animator.Play("Left");
        rightState = false;
        topState = false;
        leftState = true;
    }
    
    public void SwitchFontCamera()
    {
        RandomScrollManager.Instance.LongClickCheck = false;
        StopAllCoroutines();
        animator.Play("Font");
        canvas.gameObject.SetActive(!true);
        rightState = false;
        leftState = false;
        topState = false;
    }
    
    public void SwitchTopCamera()
    {
        UiManager.Instance.UpdateCardInventory();
        

        animator.Play("Top");
        StartCoroutine(WaitForShowCanvas());
        leftState = false;
        rightState = false;
        topState = true;
    }
    
    public void SwitchRightCamera()
    {
        if (topState)
        {
            return;
        }
        if (leftState)
        {
            leftState = false;
            animator.Play("Font");
            return;
        }
        animator.Play("Right");
        leftState = false;
        topState = false;
        rightState = true;
    }

    private IEnumerator WaitForShowCanvas()
    {
        yield return new WaitForSeconds(0.5f);
        canvas.gameObject.SetActive(true);
        RandomScrollManager.Instance.LongClickCheck = true;

        if (InventoryManager.Instance.InventoryField.gameObject.activeInHierarchy)
        {
            Debug.Log("inventory");
            RandomScrollManager.Instance.LongClickCheck = false;
        }
    }
}
